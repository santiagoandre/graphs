from collections import deque
from find_algorithms.NearestNeighbors import NearestNeighbors as NeaNe
from problems.StackBoxes import StackBoxes

print("StackBoxes problem")
boxes = ['A','B','C','D']
init_state = [['A','D','C','D','B']]
final_state = [['C','D'],['A','B']]
problem = StackBoxes(boxes,init_state,final_state)
a = NeaNe(problem)
a.run()
