# -*- coding: utf-8 -*-

from .Problem import Problem
from collections import deque


class StackBoxes(Problem):
    #A/B/C/PIZO
    def __init__(self,boxes,initial_state,final_state):
        self.boxes = boxes

        super().__init__(initial_state,final_state,None)


    def child_node(self,node):

        children = []
        movable_boxes =self.movable_boxes(node.element())#obtiene las cajas que se pueden mover
        children = self.move_boxes(movable_boxes,node)#las apila de todas las formas pocibles haciendo un movimiento
        return children + self.unstack_boxes(node) #unir con los hijos que se obtienen al desapilar el ultimo elemento de cada pila

    def move_boxes(self,boxes,node):
        if len(boxes) == 1:#solo hay una caja movible, entr estas estan las ultimas de cada apila
            #no hay donde colocar cajas
            return []
        return [self.stack(node,box,box2) for box in boxes for box2 in boxes if box2 != box]

    def unstack_boxes(self,node):
        return [self.unstack(node,stack[-1]) for stack in node.element()]

    def stack(self,node,downbox,upbox):
        new_state = []
        stacked = False
        action = None
        action2 = None
        for stack in node.element():
            if stack[-1] == upbox:
                action2 = 'stack {0} on {1}'.format(upbox,stack[-2])
            #1 mira si la caja que va a mover esta al final de la pila
                # si la pila solo tiene dos cajas, la elimina
                #sino solo quita la caja del final de la pila
                if len(stack) == 2:
                    continue# no aniade esa pila al nuevo stado
                new_stack = list(stack[:-1])
            elif stack[-1] == downbox:
            #2 mira si la caja de abajo esta al final de una pila:
                #para anadir la caja de arriba a esa pila
                new_stack = list(stack)
                new_stack.append(upbox)
                stacked = True
            else:
                new_stack = list(stack)
            new_state.append(new_stack)

        if not  stacked:
            #hay que crear una nueva pila ya que la caja de abajo no esta en ninguna pila
            new_state.append([downbox,upbox])

        if action2 is None:
            action2 = 'unstack {0}'.format(upbox)

        action ='stack {0} on {1}'.format(upbox,downbox)
        new_node = self.graph.add_node(new_state)
        node.add_adjacency(new_node,action)
        new_node.add_adjacency(node,action2)
        return new_node


    def unstack(self,node,box):
        #coloca en el piso la caja que se epecifica
        downbox = None #la caja que esta abajo de la que se va a desapilar
        new_state = []
        for stack in node.element():#reorre cada pila
            if stack[-1] == box:# si el ultimo elemento de la pila es la caja que se va a deapilar
                downbox = stack[-2]#el penultimo elemento de la pila
                if len(stack) == 2:#la pila en la que esta solo tienedos cajas
                    continue #se elimina esa pila del nuevo estado
                new_stack = stack[:-1]#la pila tiene mas de 2 elementos, solo elimina  la ultima caja

            else:
                new_stack = list(stack)
            new_state.append(new_stack)
        new_node = self.graph.add_node(new_state)
        new_node.add_adjacency(node,'stack {0} on {1}'.format(box,downbox))
        node.add_adjacency(new_node,'unstack {0}'.format(box))
        return new_node
    def movable_boxes(self,stacks):
        if not stacks:#no hay nada apilado
            return self.boxes#todas se pueden mover
        #obtiene las cajas que estan sobre el priso y se pueden mover
        #no tienen otra caja encima
        rigid_boxes = [box for stack in stacks for box in stack[:-1]]
        return [box for box in self.boxes if box not in rigid_boxes]
