# -*- coding: utf-8 -*-
from logic.graph.nodes import *
class Graph:
	#Constructor
	def __init__(self,Weighted = False):
		self._nodes = []
		self.Weighted =Weighted
		if Weighted:
			self.len_edges = 3
		else:
			self.len_edges = 2
	#geters
	def nodes(self):
		return self._nodes
	#geter de ejes, retorna los elementos
	def edges(self):
		edges = []
		nodes_cloced = []
		#print("adjacencies: ")
		for node in self.nodes():
			#print(node.adjacencies())
			for node_adjacency in node.adjacencies():
				if isinstance(node_adjacency,list):
					adjacency = (node,*node_adjacency)
				else:
					adjacency = (node,node_adjacency)
				if not self.exist_adjacency(adjacency,edges):
					edges.append(adjacency)
				if node_adjacency not in nodes_cloced:
					nodes_cloced.append(node_adjacency)

		#print("---------------")
		return edges
	#aderir nodo, resive cualquier canidad de argumentos,
	#esto se hace porque es una clase base, y las clase que heredan pueden decir que un nodo tiene mas atributos
	def add_node(self, *nodo):
		new_node = self.create_node(*nodo) # se crea un nuevo nodo
		if new_node not in self.nodes(): # si el nodo no esta en la lista lo agrega
			self.nodes().append (new_node)
			return new_node
		else:
			
			return self._get_node(*nodo)
	#eliminar nodo; resive el elemento que tiene ese nodo
	def del_node(self, element):
		try:
			#crea un nodo igual al que se va a eliminar, y se llama el elemento remove de nodos
			#ejecutando el metodo __eq__ si un nodo dde la lista es igual al que se envio por parametro, este elimina de la lista
			#sino retorna un error
			self.nodes().remove(self.create_node(element))
			return True
		except:
			return False
	#metodo privado que retorna un nodo con el elemento
	def _get_node(self, element):
		for node in	self._nodes:
			if node.element() == element:
				return node
	# añadir arista resive cualquier numero de parametros en una tupla, los cuales forman  una arista
	def add_edge(self, *edge):
		if len(edge) != self.len_edges:
			return False

		# toda arista siemper debe tener un elemento inicial y otro final
		init_element,fnl_element = edge[0],edge[1] # se obtienen en variables
		init_node = self._get_node(init_element) # se obtiene el nodo relacionado al priemr elemento
		fnl_node = self._get_node(fnl_element)# se obtiene el nodo relacioneado con el segundo elemento
		#se le dice al nodo inicial que añada una adyacencia
		if not self.Weighted:
			adjacency = fnl_node
		else:
			adjacency = fnl_node,*edge[2]
		#print("adjacency = ",adjacency)
		success = init_node.add_adjacency(adjacency) #retorna true si se añadio
		#print(init_node.adjacencies())
		return success
	# eliminar arista resive la arista en una tupla
	def del_edge(self, *edge):
		#se hace lomismo que en añadir arista
		init_element,fnl_element = edge[0],edge[1]
		init_node = self._get_node(init_element)
		fnl_node = self._get_node(fnl_element)
		print(init_node.adjacencies())
		if self.Weighted:
			adjacency = fnl_node,adjacency[2]
		else:
			adjacency = fnl_node
		success = init_node.del_adjacency(adjacency)
		return success
	def add_nodes(self, nodes):
		# un metodo que resive una lista con los elementos de varios  nodos a añadir
		repeated_nodes=[]
		for node in nodes:
			#pregunta que si el elemento es una tupla
			success = False
			if isinstance(node,tuple) :
				#se es asi se descompone
				success = self.add_node(*node)
			else:
				# sino se envia normal
				success = self.add_node(node)
			if not success:
				repeated_nodes.append(node)
		return repeated_nodes
	#añadir varias aristas
	def add_edges(self,edges):
		repeated_edges = []
		for edge in edges:
			#un eje siempre va a ser una tupla asi que siempre se descompone
			if not self.add_edge(*edge):
				repeated_edges.append(edge)
		return repeated_edges
#clase nodo dirigido exstiende de Graph
class DirectedGraph(Graph):
	#crear nodo, solo resive el elemento que tendra el nodo
	def create_node(self,element):
		#develve un nodo dirigido ponderado
		return DirectedNode(element)
	def exist_adjacency(self,adjacency,edges):
		return False # no es lo mismo (3,1) que (1,3) asi que no hay que validar nada
class UndirectedGraph(Graph):
	#crear nodo, solo resive el elemento que tendra el nodo
	def create_node(self,element):
		#develve un nodo dirigido ponderado
		return UndirectedNode(element)
	def exist_adjacency(self,adjacency,edges):
		#intercabiar nodo inicial con nodo final
		#ej: (2,3) -> (3,2) o (3,4,up) ->  (4,3,up)
		if adjacency in edges:
			return True
		if len(adjacency) == 2:
			adjacency = (adjacency[1], adjacency[0])
		else:
			adjacency  = (adjacency[1], adjacency[0],*adjacency[2:])
		return adjacency in edges
