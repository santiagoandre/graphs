# -*- coding: utf-8 -*-
#modulo nodos
#clase base Node
class Node:
	#constructor  resive el elemento que guardara
	def __init__(self, element,tag = None):
		self._element = element
		self._adjacencies = [] #inicializa la lista de adyacencias
		self.tag  = tag
	#geters
	def element(self):
		return self._element
	def adjacencies(self):
		return self._adjacencies
	#seters
	def set_element(self, new_element):
		self._element= new_element
	#to string en java
	def __str__(self):
		return str(self.element())
	# cuando es llamado el to string en una lista
	def __repr__(self):
	    return str(self)
	#comparar con otro objeto equals en java
	def __eq__(self, other):
		return self.element() == other.element()


	# dice si existe una adyacenica
	# validar adyacencia, lo unico que tiene que hacer es preguntar si no  existe la adyacencia, en ese caso es valida
	def validate_adjacency(self,adjacency):
		return adjacency not in self.adjacencies()
# clase nodo dirigido ponderado, extiende de Node

class DirectedNode(Node):

	#añadir adyacencia, resive cualquier numero de parametros y los guarda en la tupla 'adjacency'
	def add_adjacency(self,*adjacency):
		if len(adjacency) == 1:
			adjacency = adjacency[0]
		if self.validate_adjacency(adjacency): #pregunta si es valida la adyacencia
			#si es cierto construlle la adyacencia y la añade a la lista de adyacencias
			self.adjacencies().append(adjacency)
			return True
		#si no es cierto retorna false, no se realizo la adicion
		return False
	#eliminar adyacencia
	def del_adjacency(self,*adjacency):
		if len(adjacency) == 1:
			adjacency = adjacency[0]
		try:
			#crea una tupla con los valores de la adyacencia a eliminar y le dice a la lista de asyacencias que la elimine
			#esta eecutara el metodo __Eq__ para cada para cada adyacencia de la lista mandadole la asyacencia que se acabo de construir
			# las adyacencias son iguales la elimina
			self.adjacencies().remove(adjacency)
			#si no existe se lanza un error
			return True
		except:
			#el error es capturado y se retorna falso
			return False # no se elimino nada, no existia la adyacencia
class UndirectedNode(Node):
	def exist_adjacency(self, *adjacency):
		# construye la adyacencia y pregunta si esta en las adyacencias del nodo, hace lo mismo que en del_adjacency

		if adjacency  in self.adjacencies() :
			return True
	#añadir adyacencia, resive cualquier numero de parametros y los guarda en la tupla 'adjacency'
	def add_adjacency(self,*adjacency):

		if self.validate_adjacency(*adjacency): #pregunta si es valida la adyacencia
			#si es cierto construlle la adyacencia y la añade a la lista de adyacencias
			self.adjacencies().append(adjacency)
			#crea la adyacencia equivalente para el otro nodo
			other_node = adjacency[0]
			if not other_node.__eq__(self):
				adjacency = (self,*adjacency[1:])
				other_node.adjacencies().append(adjacency)
			return True
		#si no es cierto retorna false, no se realizo la adicion
		return False
	#eliminar adyacencia
	def del_adjacency(self,*adjacency):
		try:
			#crea una tupla con los valores de la adyacencia a eliminar y le dice a la lista de asyacencias que la elimine
			#esta eecutara el metodo __Eq__ para cada para cada adyacencia de la lista mandadole la asyacencia que se acabo de construir
			# las adyacencias son iguales la elimina
			self.adjacencies().remove(adjacency)

			#si no existe se lanza un error
			#crea la adyacencia equivalente para el otro nodo
			other_node = adjacency[0]
			adjacency[0] = self
			other_node.adjacencies().remove(adjacency)
			return True
		except:
			#el error es capturado y se retorna falso
			return False # no se elimino nada, no existia la adyacencia
